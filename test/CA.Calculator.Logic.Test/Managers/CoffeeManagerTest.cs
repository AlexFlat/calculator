using CA.Calculator.Basic;
using CA.Calculator.Data.Config;
using CA.Calculator.Data.Data;
using CA.Calculator.Data.Enums;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CA.Calculator.Logic.Test.Managers
{
    [TestClass]
    public class CoffeeManagerTest
    {
        private Mock<ILogger> MockLogger = new Mock<ILogger>();

        private AppSettings GetAppSettings()
        {
            return new AppSettings()
            {
                Pricing = new Pricing()
                {
                    Service = 1.3,
                    Coffee = new CoffeePricing()
                    {
                        Base = 1.5,
                        MilkFoamed = 0.5,
                        MilkLarge = 0.3,
                        MilkSmall = 0.25,
                        Sprinkles = 0.5,
                        Sugar = 0.1
                    }
                }
            };
        }

        [TestMethod]
        public void CalculateCost_WhenCoffeeIsNull_Returns0()
        {
            //Arrange
            var coffeeManager = new CoffeeManager(GetAppSettings(), MockLogger.Object);

            //Act
            var actual = coffeeManager.CalculateCost(null);

            //Assert
            Assert.AreEqual(actual, 0);
        }

        [TestMethod]
        public void CalculateCost_WhenCoffeeHasNegativeSugar_SugarCostIsZero()
        {
            //Arrange
            var coffeeManager = new CoffeeManager(GetAppSettings(), MockLogger.Object);
            var coffee = new Coffee();
            coffee.Sugar = new Sugar() { Count = -1 };

            //Act
            var actual = coffeeManager.CalculateCost(new List<Coffee>() { coffee });

            //Assert
            Assert.AreEqual(actual, 2.8);
        }

        [TestMethod]
        [DataTestMethod]
        //No Milk, No Sugar, No foam, No Sprinkles = 1.3 + 1.5 = 2.8
        [DataRow(false, CoffeeSizes.Small, 0, false, false, 2.8)]
        //Milk Small, 1 Sugar, NoFoam, No Sprinkles = 1.3 + 1.5 + 0.25 + 0.1 = 3.15
        [DataRow(true, CoffeeSizes.Small , 1, false, false, 3.15)]
        //ETC
        public void CalculateCost_WhenCoffeeIsValid_ReturnsCorrectResult(bool milk, CoffeeSizes coffeeSize, int sugar, bool isFoamed, bool sprinkles, double expected)
        {
            //Arrange
            var coffeeManager = new CoffeeManager(GetAppSettings(), MockLogger.Object);

            var coffee = new Coffee();
            if(milk)
            {
                coffee.Milk = new Milk() { IsFoamed = isFoamed };
            }
            coffee.Size = coffeeSize;
            coffee.Sugar = new Sugar() { Count = sugar };
            coffee.Extras = new Extras() { Sprinkles = sprinkles };

            //Act
            var actual = coffeeManager.CalculateCost(new List<Coffee>() { coffee });

            //Assert
            Assert.AreEqual(actual, expected);
        }
    }
}
