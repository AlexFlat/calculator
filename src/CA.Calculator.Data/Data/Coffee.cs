﻿using CA.Calculator.Data.Enums;
using System.Text;

namespace CA.Calculator.Data.Data
{
    public class Coffee
    {
        public CoffeeSizes Size { get; set; }

        public Milk Milk { get; set; }

        public Sugar Sugar { get; set; }

        public Extras Extras { get; set; }
    }
}
