﻿namespace CA.Calculator.Data.Config
{
    public class Pricing
    {
        /// <summary>
        /// The price to place an order
        /// </summary>
        public double Service { get; set; }
        /// <summary>
        /// The price for coffees
        /// </summary>
        public CoffeePricing Coffee { get; set; }

    }
}
