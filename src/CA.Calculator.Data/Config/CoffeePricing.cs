﻿namespace CA.Calculator.Data.Config
{
    /// <summary>
    /// Pricing for Coffee
    /// </summary>
    public class CoffeePricing
    {
        /// <summary>
        /// The base price of an espresso shot
        /// </summary>
        public double Base { get; set; }
        /// <summary>
        /// The price for adding milk (small cup)
        /// </summary>
        public double MilkSmall { get; set; }
        /// <summary>
        /// The price for adding milk (large cup)
        /// </summary>
        public double MilkLarge { get; set; }
        /// <summary>
        /// The price to foam the milk
        /// </summary>
        public double MilkFoamed { get; set; }
        /// <summary>
        /// The price for each sugar added
        /// </summary>
        public double Sugar { get; set; }
        /// <summary>
        /// The price to add sprinkles (probably only for milk orders?)
        /// </summary>
        public double Sprinkles { get; set; }
    }
}
