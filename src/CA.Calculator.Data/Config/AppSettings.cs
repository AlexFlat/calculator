﻿namespace CA.Calculator.Data.Config
{
    public class AppSettings
    {
        /// <summary>
        /// Pricing settings for the app
        /// </summary>
        public Pricing Pricing { get; set; }
    }
}
