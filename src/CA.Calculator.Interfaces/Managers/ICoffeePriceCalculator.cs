﻿using CA.Calculator.Data.Data;
using System.Collections.Generic;

namespace CA.Calculator.Interfaces.Managers
{
    /// <summary>
    /// Calculates the total price for Coffee orders
    /// </summary>
    public interface ICoffeePriceCalculator
    {
        /// <summary>
        /// Calculates the price for the coffees passed
        /// </summary>
        /// <param name="coffees"></param>
        /// <returns></returns>
        double CalculateCost(IList<Coffee> coffees);

    }
}
