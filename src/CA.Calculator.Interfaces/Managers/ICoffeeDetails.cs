﻿using CA.Calculator.Data.Data;

namespace CA.Calculator.Interfaces.Managers
{
    public interface ICoffeeDetails
    {
        string GetDescription(Coffee coffee);
    }
}
