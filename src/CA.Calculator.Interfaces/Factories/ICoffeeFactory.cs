﻿using CA.Calculator.Data.Data;

namespace CA.Calculator.Interfaces.Factories
{
    public interface ICoffeeFactory
    {
        Coffee Create();
    }
}
