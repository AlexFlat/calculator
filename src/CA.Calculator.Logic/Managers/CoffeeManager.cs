﻿using CA.Calculator.Data.Config;
using CA.Calculator.Data.Data;
using CA.Calculator.Interfaces.Managers;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CA.Calculator.Basic
{
    /// <summary>
    /// Performs operations relating to Coffeess
    /// </summary>
    public class CoffeeManager : ICoffeePriceCalculator, ICoffeeDetails
    {
        private AppSettings _appSettings;
        private ILogger _logger;

        public CoffeeManager(AppSettings appSettings, ILogger logger)
        {
            ValidateCoffeePricing(appSettings);
            _appSettings = appSettings;
            _logger = logger;
        }

        /// <summary>
        /// Calculates the cost for the coffees passed including the service fee
        /// </summary>
        /// <param name="coffees"></param>
        /// <returns></returns>
        public double CalculateCost(IList<Coffee> coffees)
        {
            try
            {
                var result = 0.0;
                if (coffees == null || coffees.Count == 0)
                {
                    return result;
                }

                // Add service charge of $1.30 per order
                result += _appSettings.Pricing.Service;
                foreach (var coffee in coffees)
                {
                    result += CalculateSingleCost(coffee);
                }
                return Math.Round(result, 2);
            }
            catch (Exception ex)
            {
                _logger?.LogError(ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// Returns a human readable description for the coffee
        /// </summary>
        /// <param name="coffee"></param>
        /// <returns></returns>
        public string GetDescription(Coffee coffee)
        {
            var result = new StringBuilder();
            result.Append($"{nameof(Coffee)}: ");
            if (coffee == null)
            {
                result.Append("No Coffee");                
            }    
            else
            {
                if(coffee.Milk != null)
                {
                    result.Append($" {nameof(coffee.Milk)}: {nameof(coffee.Size)}: {coffee.Size}");
                }
                else
                {
                    result.Append($" {nameof(coffee.Milk)}: None");
                }
                if (coffee.Sugar != null && coffee.Sugar.Count > 0)
                {
                    result.Append($" {nameof(coffee.Sugar)}: {coffee.Sugar.Count}");
                }
                else
                {
                    result.Append($" {nameof(coffee.Sugar)}: None");
                }
                if (coffee.Extras != null)
                {
                    result.Append($" {nameof(coffee.Extras)}: {nameof(coffee.Extras.Sprinkles)}: {coffee.Extras.Sprinkles}");
                }
                else
                {
                    result.Append($" {nameof(coffee.Extras)}: None");
                }
            }
            return result.ToString();
        }

        /// <summary>
        /// Calculates the price for a single coffee
        /// </summary>
        /// <param name="coffee"></param>
        /// <returns></returns>
        private double CalculateSingleCost(Coffee coffee)
        {
            var result = 0.0;
            if (coffee == null)
            {
                return result;
            }

            //Always add the base
            // Start with basic Expresso
            result += _appSettings.Pricing.Coffee.Base;

            //Add the cost of the milk
            //Milk 25c or 30c for large
            if (coffee.Milk != null)
            {
                switch(coffee.Size)
                {
                    case Data.Enums.CoffeeSizes.Small:
                        result += _appSettings.Pricing.Coffee.MilkSmall;
                        break;  
                    case Data.Enums.CoffeeSizes.Large:
                        result += _appSettings.Pricing.Coffee.MilkLarge;
                        break;
                    default:
                        throw new NotSupportedException($"The Coffee Size {coffee.Size} is not supported");
                }

                //Only add foaming when there is Milk
                // Milk Foaming 5c
                if(coffee.Milk.IsFoamed)
                {
                    result += _appSettings.Pricing.Coffee.MilkFoamed;
                }
            }

            //Add sugar
            // Calc sugar @ 10c each
            if (coffee.Sugar != null && coffee.Sugar.Count > 0)
            {
                result += _appSettings.Pricing.Coffee.Sugar;
            }

            //Add sprinkles to an Espresso? i guess we can allow that? Maybe need to clarify the requirement
            // chocolate sprinkles
            if (coffee.Extras != null && coffee.Extras.Sprinkles)
            {
                result += _appSettings.Pricing.Coffee.Sprinkles;
            }

            return result;
        }

        private void ValidateCoffeePricing(AppSettings appSettings)
        {
            if (appSettings == null)
            {
                throw new ArgumentNullException(nameof(appSettings));
            }
            if (appSettings.Pricing == null)
            {
                throw new ArgumentNullException(nameof(appSettings.Pricing));
            }
            if (appSettings.Pricing.Coffee == null)
            {
                throw new ArgumentNullException(nameof(appSettings.Pricing.Coffee));
            }
            ValidatePrice(0, appSettings.Pricing.Coffee.Base, nameof(appSettings.Pricing.Coffee.Base));
            ValidatePrice(0, appSettings.Pricing.Coffee.MilkFoamed, nameof(appSettings.Pricing.Coffee.MilkFoamed));
            ValidatePrice(0, appSettings.Pricing.Coffee.MilkLarge, nameof(appSettings.Pricing.Coffee.MilkLarge));
            ValidatePrice(0, appSettings.Pricing.Coffee.MilkSmall, nameof(appSettings.Pricing.Coffee.MilkSmall));
            ValidatePrice(0, appSettings.Pricing.Coffee.Sprinkles, nameof(appSettings.Pricing.Coffee.Sprinkles));
            ValidatePrice(0, appSettings.Pricing.Coffee.Sugar, nameof(appSettings.Pricing.Coffee.Sugar));
        }

        private void ValidatePrice(double minimum, double value, string name)
        {
            if (value < minimum)
            {
                throw new ValidationException($"The {name} has value {value} which must be greater than {minimum}");
            }
        }
    }
}
