﻿using CA.Calculator.App.Factories;
using CA.Calculator.Basic;
using CA.Calculator.Data.Config;
using CA.Calculator.Interfaces.Factories;
using CA.Calculator.Interfaces.Managers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace CA.Calculator.App.IoC
{
    public static class ContainerHelper
    {
        public static void RegisterServices(this IServiceCollection serviceContainer, AppSettings appSettings)
        {
            serviceContainer.AddSingleton<AppSettings>((c) => { return appSettings; });
            //For now just use a basic logger.. for prod would be appinsights
            serviceContainer.AddSingleton<ILogger>(new Microsoft.Extensions.Logging.Debug.DebugLoggerProvider().CreateLogger(nameof(CoffeeManager)));
            serviceContainer.AddTransient<ICoffeeFactory, ConsoleCoffeeFactory>();
            serviceContainer.AddTransient<ICoffeePriceCalculator, CoffeeManager>();
            serviceContainer.AddTransient<ICoffeeDetails, CoffeeManager>();
        }
    }
}
