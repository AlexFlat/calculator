﻿using CA.Calculator.Data.Data;
using CA.Calculator.Data.Enums;
using CA.Calculator.Interfaces.Factories;
using Microsoft.Extensions.Logging;
using System;

namespace CA.Calculator.App.Factories
{
    /// <summary>
    /// Creates a coffee from the Console from human input(s)
    /// </summary>
    public class ConsoleCoffeeFactory : ICoffeeFactory
    {
        private ILogger _logger;
        public ConsoleCoffeeFactory(ILogger logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Creates a new Coffee by prompting the user
        /// </summary>
        /// <returns></returns>
        public Coffee Create()
        {
            var result = new Coffee();
            //Console.WriteLine("Milk? (true/false)");
            if(GetBoolFromConsole("Milk? (true/false)"))
            {
                result.Milk = new Milk();

                //Large/Small is only when milk is required
                //Console.WriteLine("Large? (true/false)");
                if (GetBoolFromConsole("Large? (true/false)"))
                {
                    result.Size = CoffeeSizes.Large;
                }
                else
                {
                    result.Size = CoffeeSizes.Small;
                }
            }

            //Console.WriteLine("How many sugars?");
            result.Sugar = new Sugar() { Count = GetIntFromConsole("How many sugars?") };

            //Console.WriteLine("Foam the milk? (true/false)");
            if(result.Milk != null)
            {
                result.Milk.IsFoamed = GetBoolFromConsole("Foam the milk? (true/false)");
            }

            //Console.WriteLine("Add chocolate sprinkles? (true/false)");
            result.Extras = new Extras() { Sprinkles = GetBoolFromConsole("Add chocolate sprinkles? (true/false)") };

            return result;
        }

        private int GetIntFromConsole(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                var cmdChars = Console.ReadLine();
                if (int.TryParse(cmdChars, out var result))
                {
                    if(result >= 0)
                    {
                        return result;
                    }
                    var error = $"The input {cmdChars} must be greater or equal to 0. Please enter a valid number";
                    LogError(error);
                }
                else
                {
                    var error = $"The input {cmdChars} was invalid. Please enter \"true\" or \"false\"";
                    LogError(error);
                }
            }
        }

        private bool GetBoolFromConsole(string message)
        {
            while(true)
            {
                Console.WriteLine(message);
                var cmdChars = Console.ReadLine();
                if (bool.TryParse(cmdChars, out var result))
                {
                    return result;
                }
                else
                {
                    var error = $"The input {cmdChars} was invalid. Please enter \"true\" or \"false\"";
                    LogError(error);
                }
            }
        }

        private void LogError(string message)
        {
            if(string.IsNullOrWhiteSpace(message))
            {
                return;
            }
            _logger.LogError(message);
            var colour = Console.ForegroundColor;
            try
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(message);
            }
            finally
            {
                Console.ForegroundColor = colour;
            }
        }
    }
}
