﻿using CA.Calculator.App.IoC;
using CA.Calculator.Data.Config;
using CA.Calculator.Data.Data;
using CA.Calculator.Interfaces.Factories;
using CA.Calculator.Interfaces.Managers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace CA.Calculator.App
{
    class Program
    {
        private static IServiceProvider _serviceProvider;

        static void Main(string[] args)
        {
            IConfiguration Configuration = new ConfigurationBuilder()
              .AddJsonFile("appsettings.json", optional: false)
              .AddCommandLine(args)
              .Build();

            var appSetings = new AppSettings();
            Configuration.Bind(appSetings);

            var serviceCollection = new ServiceCollection();
            serviceCollection.RegisterServices(appSetings);
            _serviceProvider = serviceCollection.BuildServiceProvider();
            var logger = _serviceProvider.GetService<ILogger>();
            try
            {

                var coffeeFactory = _serviceProvider.GetService<ICoffeeFactory>();
                var coffeePriceCalculator = _serviceProvider.GetService<ICoffeePriceCalculator>();
                var coffeeDetails = _serviceProvider.GetService<ICoffeeDetails>();
                var coffee = coffeeFactory.Create();
                var price = coffeePriceCalculator.CalculateCost(new List<Coffee>() { coffee });
                var details = coffeeDetails.GetDescription(coffee);
                Console.WriteLine($"Coffee Price for the Coffee [{details}] is {price.ToString("C")}");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                logger.LogError(ex.ToString());
                Console.WriteLine("Something went wrong");
            }
        }
    }
}
